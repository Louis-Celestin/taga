import {useState,React} from 'react';
import "../../styles/cartStyle/card.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {useCart} from 'react-use-cart'
import {listSignaletique} from '../datas/Data.js' 
const Card = ({liste,handleClick}) => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  <Modal show={show} onHide={handleClose}>
          
          <Modal.Body><i class="fa-regular fa-circle-check"></i></Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
    //on passe les champs de typeSignalitique en props
    const {id,image,title,quantity,price}= liste ;
       // const {
    //     isEmpty,
    //     totalUniqueItems,
    //     items,
    //     updateItemQuantity,
    //     removeItem,
    //   } = useCart();
    
    //   if (isEmpty) return <p>Votre panier est vide</p>;
  return (
//     <div>
//          <h1>Cart ({totalUniqueItems})</h1>

// <ul>
//   {items.map((item) => (
//     <li key={item.id}>
//       {item.quantity} x {item.name} &mdash;
//       <button
//         onClick={() => updateItemQuantity(item.id, item.quantity - 1)}
//       >
//         -
//       </button>
//       <button
//         onClick={() => updateItemQuantity(item.id, item.quantity + 1)}
//       >
//         +
//       </button>
//       <button onClick={() => removeItem(item.id)}>&times;</button>
//     </li>
//   ))}
// </ul>
//     </div>
<div>


            <div  className="list-type-signaletique" class="card">
              <div className="image-signaletique">
                <img src={image} alt="image" />
              </div>

              <div class="card-body">
                <span className="title-signaletique">{title}</span> 
                <span className="price">{price}</span>
                <button className="btn-panier"onClick= {()=> handleClick(liste) }>
                  Ajouter au panier
                </button>
              </div>
            </div>
          </div>
  )
}

export default Card