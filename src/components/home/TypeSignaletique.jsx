import {useState,React} from "react";
import { listSignaletique } from "../datas/Data.js";
import "../../styles/homeStyle/typeSignaletique.css";
import { useCart } from "react-use-cart";
import Card from "../cart/Card.jsx";

const TypeSignaletique = () => {

  const [cart, setCart] = useState([]);

const handleClick= (liste)=>{
  cart.push(liste);
  console.log(liste);
}
  return (
    <div className="container-type-signaletique">
      <span className="title-desc-signaletique">Nos signalétiques</span>
      <p className="description-signaletique">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>

      <div className="list-signaletique">
        <a href="/signaletique" className="type-signaletique">
          Signalétique routière
        </a>
        <a href="/signaletique" className="type-signaletique">
          Signalétique publicitaire
        </a>
        <a href="/signaletique" className="type-signaletique">
          Signalétique directionnelle
        </a>
      </div>
      <section>
        {listSignaletique.map((liste) => <Card key={liste.id} liste={liste} />)}
      </section>
    </div>
  );
};

export default TypeSignaletique;
