import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "../../styles/style1/inscription.css";

const Inscription = () => {
  const [inputs, setInputs] = useState({});

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(inputs);
  };

  return (
    <div className="inscription-form-container">
      <Form>
      <h1 className='title-contact'>Inscrivez-vous</h1>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            placeholder="Nom"
            name="lastname"
            value={inputs.lastname}
            onChange={handleChange}
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            name="firstname"
            value={inputs.firstname}
            onChange={handleChange}
            placeholder="Prénom"
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="email"
            value={inputs.email}
            name="email"
            onChange={handleChange}
            placeholder="Email"
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            placeholder="Statut Social"
            value={inputs.statut_social}
            name="statut_social"
            onChange={handleChange}
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            placeholder="Registre de commerce"
            value={inputs.registre}
            name="registre"
            onChange={handleChange}
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            placeholder="Addresse"
            value={inputs.registre}
            name="registre"
            onChange={handleChange}
            className="input-text-control"
          />
        </Form.Group>
        <Form.Group className="input-text">
          <Form.Control
            type="text"
            placeholder="Mot de passe"
            className="input-text-control"
          />
        </Form.Group>
        <button type="submit" className='btn-submit'>
          Envoyer
        </button>
      </Form>
    </div>
  );
};

export default Inscription;
