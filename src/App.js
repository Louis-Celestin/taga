import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState, React } from "react";
import Home from "./components/home/Home";
import Header from "./components/Header";
import TypeSignaletique from "./components/home/TypeSignaletique";
import DemandeProcess from "./components/home/DemandeProcess";
import ContactFooter from "./components/home/ContactFooter";
import Inscription from "./components/components1/Inscription";
import Cart from "./pages/cart/Cart";
import { Routes, Route } from "react-router-dom";
import Connexion from "./components/components1/Connexion";
import Acceuil from "./pages/Acceuil/Acceuil";
import CartDetail from "./pages/cart/CartDetail";

function App() {
  const [show, setShow] = useState(true);
  const [cart, setCart] = useState([]);
  const handleClick = (liste) => {
    setCart([...cart, liste]);
  };

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Acceuil />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/cart/:id" element={<CartDetail />} />
      </Routes>
    </div>
  );
}

export default App;
