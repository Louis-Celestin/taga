import React from "react";
import ContactFooter from "../../components/home/ContactFooter";
import DemandeProcess from "../../components/home/DemandeProcess";
import Home from "../../components/home/Home";
import TypeSignaletique from "../../components/home/TypeSignaletique";

export default function Acceuil() {
  return (
    <div>
      <Home />
      <TypeSignaletique />
      <DemandeProcess />
      <ContactFooter />
    </div>
  );
}
