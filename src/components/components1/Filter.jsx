import React, { useState } from 'react';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import 'bootstrap/dist/css/bootstrap.min.css';
import {listSignaletique} from '../datas/Data.js'


const Filter = () => {

  const [contacts, setContacts] = useState(listSignaletique);
  const [search, setSearch] = useState('');
  return (
    <div>  <Container>
    <h1 className='text-center mt-4'>Contact Keeper</h1>
    <Form>
      <InputGroup className='my-3'>

        {/* onChange for search */}
        <Form.Control
          onChange={(e) => setSearch(e.target.value)}
          placeholder='Signalétique'
        />
      </InputGroup>
    </Form>
    <Table striped bordered hover>
   

        {listSignaletique
          .filter((liste) => {
            return search.toLowerCase() === ''
              ? liste
              : liste.title.toLowerCase().includes(search);
          })
          .map((liste) => (
            <div  key={liste.id} className='list-type-signaletique' >
           
            <div >
                <span >{liste.title}</span>
            </div>

        </div>
          ))}
    </Table>
  </Container>
    </div>
  )
}

export default Filter
