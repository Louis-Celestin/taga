import React from "react";

export default function Separator(props) {
  return <div style={{ width: props.width, height: props.height }}></div>;
}
