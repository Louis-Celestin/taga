import React from "react";
import style from "./Datatable.module.css";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { listSignaletique } from "../../components/datas/Data";
export default function Datatable() {
  return (
    <table className={style.Table}>
      <thead>
        <th>Produit</th>
        <th>Prix</th>
        <th>Quantité</th>
        <th>Total</th>
      </thead>
      <tbody>
        {listSignaletique.map((item) => {
          return (
            <tr>
              <td>
                <div className={style.CartImageInfo}>
                  <div className={style.CartItemImage}>
                    <img
                      style={{ width: "100%", height: "auto" }}
                      src={item.image}
                      alt=""
                    />
                  </div>
                  <div className={style.CartItemInfo}>
                    <p>{item.title}</p>
                    <p className={style.CartItemInfo__btnDelete}>Supprimer</p>
                  </div>
                </div>
              </td>
              <td className={style.CartPrice}>{item.price}</td>
              <td className={style.CartQuatity}>
                <div className={style.CartQuatity__Conatiner}>
                  <AddIcon />
                  <p>{item.quantity}</p>
                  <RemoveIcon />
                </div>
              </td>
              <td className={style.CartPrice}>{item.price}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
