import { React, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "../../styles/style1/connexion-page.css";
import { Link } from "react-router-dom";

function Connexion() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>
          <p className="mt-5 text-muted">Connectez-moi à votre compte</p>
          <Form id="sign-in-form" className="text-center p-3 w-100">
            <Form.Group controlId="sign-in-email-address">
              <Form.Control
                type="email"
                size="lg"
                placeholder="Email address"
                autoComplete="username"
                className="position-relative"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="sign-in-password">
              <Form.Control
                type="password"
                size="lg"
                placeholder="Password"
                autoComplete="current-password"
                className="position-relative"
              />
            </Form.Group>

            <div className="d-grid">
              <button type="submit" className="btn-submit">
                Se connecter
              </button>
            </div>
            <p className="mt-5 text-muted">
              Pas encore de compte ?<Link to="/signin">Inscrivez-vous</Link>{" "}
            </p>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}
export default Connexion;
