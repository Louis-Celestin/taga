import React, { useState } from 'react';

import '../../styles/homeStyle/contact-footer.css';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';

const ContactFooter= () => {
  const [inputs, setInputs] = useState({});


    const handleChange = (event) => {
      const name = event.target.name;
      const value = event.target.value;
      setInputs(values => ({...values, [name]: value}))
    }
  
    const handleSubmit = (event) => {
      event.preventDefault();
      console.log(inputs);
    }

  return (
    <div className='form-container'>

      <div className='contact'>
      <h1 className='title-contact'>Contactez-nous</h1>
        <p className='description-contact'>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
       <Form onSubmit={handleSubmit} >
      <Row className="mb-3 imput-1">
        <Form.Group as={Col} md="6" >
     
          <Form.Control
            required
            type="text"
            name='lastname'
            value={inputs.lastname}
            onChange={handleChange}
            placeholder="Nom"
            className='input-lastname'
          />

          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="6" controlId="validationCustom02">

          <Form.Control
            required
            type="text"
            name='firstname'
            value={inputs.firstname}
            onChange={handleChange}
            placeholder="Prénom"
            className='input-firstname'
          />
        </Form.Group>
      </Row>
      <Row className="mb-3">
        
        <Form.Group as={Col} md="12">

          <Form.Control type="text" placeholder="Email" required   name='email' value={inputs.email} onChange={handleChange} className='input-email'/>
         
        </Form.Group>

      </Row>

        <Form.Group as={Col} md="12">

          <Form.Control as='textarea'
            required
            placeholder="Message"
            name='textarea' value={inputs.textarea} onChange={handleChange}
          />

          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>

        </Form.Group>

      <button type="submit" className='btn-submit'>Enregistrer</button>
    </Form>
    </div>
   
  );
}

export default ContactFooter;