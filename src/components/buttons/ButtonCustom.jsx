import React from "react";
import style from "./ButtonCustom.module.css";
export default function ButtonCustom(props) {
  return (
    <div className={style.ButtonCustom} onClick={props.callback}>
      {props.title ?? "Button"}
    </div>
  );
}
