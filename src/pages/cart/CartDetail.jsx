import React, { useState } from "react";
import style from "./CartDetail.module.css";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import ButtonCustom from "../../components/buttons/ButtonCustom";
import Separator from "../../utils/Separator/Separator";
export default function CartDetail() {
  const [quatity, setquatity] = useState(1);
  return (
    <div>
      <div className={style.CartDetail}>
        <div className={style.CartDetailImage}></div>
        <div className={style.CartDetailInfo}>
          <h3>Signalétique rectangle directionnel de type 1</h3>
          <p className={style.CartDetailInfo__price}>3500 Fcfa</p>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore
            debitis odio quo, architecto fuga perferendis deserunt assumenda,
            nihil vel quisquam tempora cupiditate, hic molestiae nostrum
            praesentium sunt. In, temporibus ea? Lorem ipsum dolor sit amet
            consectetur, adipisicing elit. Tempore debitis odio quo, architecto
            fuga perferendis deserunt assumenda, nihil vel quisquam tempora
            cupiditate, hic molestiae nostrum praesentium sunt. In, temporibus
            ea?
          </p>
          <ArrowForwardIcon />
          Etape 2: Personnaliser votre signalétique selon votre goût
          <Separator height={20} />
          <div
            style={{ display: "flex", alignItems: "center", flexWrap: "wrap" }}
          >
            <div style={{ fontSize: 25, fontWeight: "700", marginRight: 10 }}>
              Quatité :{" "}
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
              }}
            >
              <div
                style={{
                  border: "1px solid black",
                  padding: 20,
                  boxSizing: "border-box",
                }}
              >
                {quatity}
              </div>
              <div style={{ marginLeft: 10 }}>
                <div
                  onClick={() => setquatity(quatity + 1)}
                  style={{
                    background: "#F4F4F4",
                    borderBottom: "0.5px solid #BEBEBE",
                    cursor: "pointer",
                  }}
                >
                  <KeyboardArrowUpIcon />
                </div>
                <div
                  onClick={() => setquatity(quatity - 1)}
                  style={{
                    background: "#F4F4F4",
                    borderTop: "0.5px solid #BEBEBE",
                    cursor: "pointer",
                  }}
                >
                  <KeyboardArrowDownIcon />
                </div>
              </div>
              <Separator width={30} />
              <div className={style.CartDetailBtn}>
                <ButtonCustom title={"Personnaliser"} />
                <Separator width={20} />
                <ButtonCustom title={"Commander"} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
