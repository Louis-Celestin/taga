import React, { useState } from "react";
import "../styles/header.css";
import { Link } from "react-router-dom";
import Divider from "@material-ui/core/Divider";
import logo from "../assets/logo.png";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Vector from "../assets/Vector.svg";
import ph_user_plus from "../assets/ph_user_plus.svg";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MenuIcon from "@mui/icons-material/Menu";
//import "../styles/style1/connexion-page.css";

const Header = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // material icone modal
  // const [open, setOpen] = React.useState(false);
  // const handleOpen = () => setOpen(true);
  // const handleClose = () => setOpen(false);
  const [inputs, setInputs] = useState({});
  const showMenu = () => {
    let menuBar = document.querySelector(".nav-component");
    menuBar.classList.toggle("showMenu");
  };

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(inputs);
  };
  return (
    <div className="nav-component">
      {/* <Modal show={show} onHide={handleClose}>
        
        <Modal.Body><i class="fa-regular fa-circle-check"></i></Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>
          <div className="title-connexion">Connectez-vous à votre compte</div>
          <Form
            id="sign-in-form"
            className="text-center p-3 w-100"
            onSubmit={handleSubmit}
          >
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="text"
                  placeholder="Email"
                  required
                  name="email"
                  onChange={handleChange}
                  className="input-email"
                  value={inputs.email}
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  required
                  value={inputs.password}
                  name="password"
                  onChange={handleChange}
                  className="input-password"
                />
              </Form.Group>
            </Row>

            <div className="div-copyright">
              <button type="submit" className="btn-submit">
                Se connecter
              </button>
            </div>
            <p className="mt-5 text-muted">
              Pas encore de compte ? Inscrivez-vous !
            </p>
          </Form>
        </Modal.Body>
      </Modal>

      {/* <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box >
          <Typography id="modal-modal-title" variant="h6" component="h2">
          <div className="title-connexion">Connectez-vous à votre compte</div>
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
          <Form
            id="sign-in-form"
            className="text-center p-3 w-100"
            onSubmit={handleSubmit}
          >
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="text"
                  placeholder="Email"
                  required
                  name="email"
                  onChange={handleChange}
                  className="input-email"
                  value={inputs.email}
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  required
                  value={inputs.password}
                  name="password"
                  onChange={handleChange}
                  className="input-password"
                />
              </Form.Group>
            </Row>

            <div className="div-copyright">
            <button type="submit" className='btn-submit'>
          Se connecter
        </button>
            </div>
            <p className="mt-5 text-muted">Pas encore de compte ? Inscrivez-vous !</p>
          </Form>
          </Typography>
        </Box>
      </Modal> */}

      <div className="nav-head">
        <Link to={"/"}>
          <div>
            <img src={logo} alt="logo" />
          </div>
        </Link>
        <div className="search">
          <i class="fa-solid fa-magnifying-glass"></i>
          <input type="search" placeholder="RECHERCHER" />
        </div>
        <div className="person-icon">
          <div class="dropdown">
            <button id="myBtn" class="dropbtn">
              <img src={ph_user_plus} alt="ph_user_plus" />
            </button>

            <div id="myDropdown" class="dropdown-content">
              <div class="btn-dropdown">
                <button>
                  <Link to="/signin">Créer un compte</Link>
                </button>
              </div>
              <div class=" btn-dropdown">
                <button onClick={handleShow}>Connexion</button>
              </div>
            </div>
          </div>
          <Divider orientation="vertical" flexItem />
          <Link to={"/cart"}>
            <div>
              <img src={Vector} alt="Vector-shopping" />
            </div>
          </Link>
        </div>
        <div className="menuBar" onClick={showMenu}>
          <MenuIcon />
        </div>
      </div>
      <nav>
        <Link to="#" className="item">
          Acceuil
        </Link>
        <Link to="#signaletique" className="item">
          Les signalétiques
        </Link>
        <Link to="#" className="item">
          Mes commandes
        </Link>
        <Link to="#" className="item">
          Contactez-nous
        </Link>
      </nav>
      <Divider variant="inset" className="divider" />
    </div>
  );
};

export default Header;
