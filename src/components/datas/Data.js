import panneau5 from '../../assets/panneau5.png';
import panneau1 from '../../assets/panneau1.png'
import panneau3 from '../../assets/panneau3.png'
import Group19 from '../../assets/Group19.png'
import Group20 from '../../assets/Group20.png'

export const listSignaletique = [
    {
        id:1,
        image:panneau5,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:1
    },
    {
        id:2,
        image:panneau1,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:3
    },
    {
        id:3,
        image:panneau3,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:5
    },
    {
        id:4,
        image:Group19,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:3
    },
    {
        id:5,
        image:Group20,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:2
    },
    {
        id:6,
        image:Group19,
        title:'Signaletique directionnel de type1',
        price:"35000F",
        quantity:3
    },
]